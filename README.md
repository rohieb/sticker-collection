rohieb's personal sticker collection
------------------------------------

All stickers in this repository have been designed by me.
The designs are available under the CC0 Public Domain Dedication:
<https://creativecommons.org/publicdomain/zero/1.0/>.
Feel free to print them and distribute them however you like.

For any further questions:
- Mail: stickers at rohieb.name
- Fediverse: <https://chaos.social/@daniel_bohrer>
